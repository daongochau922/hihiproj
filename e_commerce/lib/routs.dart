import 'package:e_commerce/animation/main_animation.dart';
import 'package:e_commerce/chat_app/screen/chat_page/chat_screen.dart';
import 'package:e_commerce/chat_app/screen/chats_page/chats_page.dart';
import 'package:e_commerce/chat_app/screen/forgot_password_chat.dart/forgot_pass.dart';
import 'package:e_commerce/chat_app/screen/register_screen.dart/register_screen.dart';
import 'package:e_commerce/chat_app/screen/sign_in/signin_chat_screen.dart';
import 'package:e_commerce/e_commerce/screens/profile/profile_screen.dart';
import 'package:e_commerce/e_commerce/screens/sign_in/sign_in_screen.dart';
import 'package:e_commerce/menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'e_commerce/screens/cart/cart_screen.dart';
import 'e_commerce/screens/complete_profile/complete_profile_screen.dart';
import 'e_commerce/screens/details/details_screen.dart';
import 'e_commerce/screens/forgot_password/forgot_password_screen.dart';
import 'e_commerce/screens/home_page/home_page_screen.dart';
import 'e_commerce/screens/login_success/login_success_screen.dart';
import 'e_commerce/screens/otp/otp_screen.dart';
import 'e_commerce/screens/sign_up/sign_up_screen.dart';
import 'e_commerce/screens/splash/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  LoginSuccess.routeName: (context) => LoginSuccess(),
  HomePageScreen.routeName: (context) => HomePageScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  ConpleteProfileScreen.routeName: (context) => ConpleteProfileScreen(),
  OtpScreen.routeName: (context) => OtpScreen(),
  DetailsScreen.routeName: (context) => DetailsScreen(),
  CartScreen.routeName: (context) => CartScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  Menu.routeName: (context) => Menu(),
  AnimationScreen.routeName: (context) => AnimationScreen(),

  //chat app
  MainChatApp.routeName: (context) => MainChatApp(),
  ChatScreen.routeName: (context) => ChatScreen(),
  SignInChatScreen.routeName: (context) => SignInChatScreen(),
  RegisterChatScreen.routeName: (context) => RegisterChatScreen(),
  ForgotPasswordChatScreen.routeName: (context) => ForgotPasswordChatScreen(),
};
