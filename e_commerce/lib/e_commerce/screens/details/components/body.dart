import 'package:e_commerce/e_commerce/components/default_button.dart';
import 'package:e_commerce/e_commerce/model/product.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

import 'color_card.dart';
import 'description.dart';
import 'product_preview.dart';
import 'rounded_icon_button.dart';
import 'top_rounded_container.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ProductPreview(product: product),
          SizedBox(
              height:
                  getProportionateScreenHeight(SizeConfig.screenHeight * 0.02)),
          TopRoundedContainer(
            color: Colors.white,
            child: Column(
              children: [
                Description(product: product),
                TopRoundedContainer(
                  color: Color(0xFFF5F6F9),
                  child: Column(
                    children: [
                      Padding(
                        padding:
                            EdgeInsets.all(getProportionateScreenWidth(20)),
                        child: Row(
                          children: [
                            ColorCard(product: product),
                            Expanded(child: SizedBox()),
                            RoundedIconButton(
                              icondata: Icons.add,
                              press: () {},
                              size: 40,
                            ),
                            SizedBox(
                              width: getProportionateScreenWidth(10),
                            ),
                            RoundedIconButton(
                              icondata: Icons.add,
                              press: () {},
                              size: 40,
                            ),
                          ],
                        ),
                      ),
                      TopRoundedContainer(
                        color: Colors.white,
                        child: Padding(
                          padding:
                              EdgeInsets.all(getProportionateScreenWidth(20)),
                          child: DefaultButton(
                            text: 'Add to Card',
                            onPressed: () {},
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
