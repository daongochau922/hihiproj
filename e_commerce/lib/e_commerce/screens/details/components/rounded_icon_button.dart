import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class RoundedIconButton extends StatelessWidget {
  const RoundedIconButton({
    Key key,
    @required this.icondata,
    @required this.press,
    @required this.size,
  }) : super(key: key);

  final IconData icondata;
  final Function press;
  final double size;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getProportionateScreenWidth(size),
      height: getProportionateScreenWidth(size),
      decoration: BoxDecoration(
        // shape: BoxShape.circle,
        borderRadius: BorderRadius.circular(25),
        color: Colors.white,
      ),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        onPressed: press,
        padding: EdgeInsets.all(
          getProportionateScreenWidth(5),
        ),
        child: Icon(icondata),
      ),
    );
  }
}
