import 'package:e_commerce/contants.dart';
import 'package:e_commerce/e_commerce/model/product.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class ProductPreview extends StatefulWidget {
  const ProductPreview({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  _ProductPreviewState createState() => _ProductPreviewState();
}

class _ProductPreviewState extends State<ProductPreview> {
  int selectedIndex = 0;
  bool isSeeMore = false;
  int numProductPreviewCanSee = 2;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          height: SizeConfig.screenHeight * 0.23,
          child: Hero(
            tag: widget.product.id,
            child: AspectRatio(
              aspectRatio: 1,
              child: Image.asset(
                widget.product.images[selectedIndex],
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal:
                getProportionateScreenWidth(SizeConfig.screenWidth * 0.1),
          ),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ...List.generate(
                  widget.product.images.length,
                  (index) => index < numProductPreviewCanSee
                      ? smallPreviewCard(index)
                      : seeMore(
                          onPress: () {
                            setState(
                              () {
                                numProductPreviewCanSee =
                                    widget.product.images.length;
                              },
                            );
                          },
                        ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget seeMore({GestureTapCallback onPress}) {
    if (isSeeMore == false) {
      isSeeMore = !isSeeMore;
      return GestureDetector(
        onTap: onPress,
        child: Container(
          width: getProportionateScreenWidth(48),
          height: getProportionateScreenWidth(48),
          child: Stack(
            children: [
              Image.asset(
                widget.product.images[widget.product.images.length - 1],
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.black.withOpacity(0.5),
                ),
              ),
              Center(
                child: Icon(
                  Icons.add,
                  color: Colors.white.withOpacity(0.6),
                  size: getProportionateScreenWidth(26),
                ),
              ),
            ],
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
            border: Border.all(
              color: Colors.white,
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  GestureDetector smallPreviewCard(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedIndex = index;
          isSeeMore = false;
        });
      },
      child: Container(
        width: getProportionateScreenWidth(48),
        height: getProportionateScreenWidth(48),
        padding: EdgeInsets.all(
          getProportionateScreenWidth(5),
        ),
        margin: EdgeInsets.only(right: 10),
        child: Image.asset(widget.product.images[index]),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          border: Border.all(
            color: selectedIndex == index ? kPrimaryColor : Colors.white,
          ),
        ),
      ),
    );
  }
}
