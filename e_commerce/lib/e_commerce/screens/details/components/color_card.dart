import 'package:e_commerce/contants.dart';
import 'package:e_commerce/e_commerce/model/product.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class ColorCard extends StatefulWidget {
  const ColorCard({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  _ColorCardState createState() => _ColorCardState();
}

class _ColorCardState extends State<ColorCard> {
  int selectedColor = 0;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ...List.generate(
          widget.product.colors.length,
          (index) => roundedCricelColor(index),
        )
      ],
    );
  }

  GestureDetector roundedCricelColor(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedColor = index;
        });
      },
      child: Container(
        width: getProportionateScreenWidth(40),
        height: getProportionateScreenWidth(40),
        padding: EdgeInsets.all(getProportionateScreenWidth(7)),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(
            color: selectedColor == index ? kPrimaryColor : Colors.transparent,
          ),
          shape: BoxShape.circle,
        ),
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: widget.product.colors[index],
            shape: BoxShape.circle,
          ),
        ),
      ),
    );
  }
}
