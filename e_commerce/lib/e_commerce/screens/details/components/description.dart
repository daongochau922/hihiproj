import 'package:e_commerce/contants.dart';
import 'package:e_commerce/e_commerce/model/product.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Description extends StatelessWidget {
  const Description({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(getProportionateScreenWidth(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            product.title,
            style: TextStyle(
              fontSize: getProportionateScreenWidth(18),
            ),
            textAlign: TextAlign.left,
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              padding: EdgeInsets.all(getProportionateScreenWidth(15)),
              width: getProportionateScreenWidth(64),
              child: SvgPicture.asset(
                'assets/icons/Heart Icon_2.svg',
                color: product.isFavourite
                    ? Color(0xFFFF4848)
                    : Colors.black.withOpacity(0.1),
              ),
              decoration: BoxDecoration(
                color: Color(0xFFFFE6E6),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(
                    getProportionateScreenWidth(30),
                  ),
                  bottomLeft: Radius.circular(
                    getProportionateScreenWidth(30),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              right: getProportionateScreenWidth(64),
            ),
            child: Text(
              product.description,
              softWrap: true,
              style: TextStyle(
                color: Colors.black.withOpacity(0.3),
              ),
              maxLines: 3,
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(10),
          ),
          Text(
            'See More Detail >',
            style: TextStyle(
              color: kPrimaryColor,
              fontSize: getProportionateScreenWidth(14),
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }
}
