import 'package:e_commerce/e_commerce/model/product.dart';
import 'package:e_commerce/e_commerce/screens/details/components/body.dart';
import 'package:e_commerce/e_commerce/screens/details/components/custom_appbar.dart';
import 'package:flutter/material.dart';

class DetailsScreen extends StatelessWidget {
  static String routeName = '/detailsScreen';
  @override
  Widget build(BuildContext context) {
    final ProductDetailArguments arguments =
        ModalRoute.of(context).settings.arguments;
    final Product product = arguments.product;
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F9),
      appBar: CustomAppBar(
        rating: product.rating,
      ),
      body: Body(product: product),
    );
  }
}

class ProductDetailArguments {
  final Product product;

  ProductDetailArguments({@required this.product});
}
