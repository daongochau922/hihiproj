import 'package:e_commerce/e_commerce/components/custom_suffix_icon.dart';
import 'package:e_commerce/e_commerce/components/default_button.dart';
import 'package:e_commerce/e_commerce/screens/otp/otp_screen.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class ConpleteProfileScreen extends StatelessWidget {
  static String routeName = '/completeProfile';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign Up'),
      ),
      body: ListView(
        children: [
          Text(
            'Compete Profile',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: getProportionateScreenWidth(30),
              color: Colors.black,
            ),
          ),
          Text(
            'Complete your detail or continue \nwith social media',
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: SizeConfig.screenHeight * 0.03,
          ),
          Form(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: 'First Name',
                        hintText: 'Enter your first name',
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        suffixIcon:
                            CustomSuffixIcon(svgIcon: 'assets/icons/User.svg')),
                  ),
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.02,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Last Name',
                        hintText: 'Enter your last name',
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        suffixIcon:
                            CustomSuffixIcon(svgIcon: 'assets/icons/User.svg')),
                  ),
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.02,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Phone number',
                        hintText: 'Enter your phone number',
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        suffixIcon: CustomSuffixIcon(
                            svgIcon: 'assets/icons/Phone.svg')),
                  ),
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.02,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Address',
                        hintText: 'Enter your address',
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        suffixIcon: CustomSuffixIcon(
                            svgIcon: 'assets/icons/Location point.svg')),
                  ),
                  SizedBox(
                    height: SizeConfig.screenHeight * 0.03,
                  ),
                  DefaultButton(
                    text: 'Continue',
                    onPressed: () {
                      Navigator.pushNamed(context, OtpScreen.routeName);
                    },
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: SizeConfig.screenHeight * 0.01,
          ),
          Text(
            'By continuing your confirm that you agree \nwith our Tern and condition',
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
