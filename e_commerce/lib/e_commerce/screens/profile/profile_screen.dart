import 'package:e_commerce/contants.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  static String routeName = '/profile';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
        backgroundColor: kPrimaryColor,
        title: Text(
          'Profile',
          style: TextStyle(
              color: Colors.white,
              fontSize: getProportionateScreenWidth(16),
              fontWeight: FontWeight.bold),
        ),
        actions: [
          FlatButton(
            onPressed: () {},
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Text(
              'Edit',
              style: TextStyle(
                fontSize: getProportionateScreenWidth(16),
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
      // body: Ticket(),
      body: Column(
        children: [
          Stack(
            children: [
              ClipPath(
                child: Container(
                  height: SizeConfig.screenHeight * 0.15,
                  color: kPrimaryColor,
                ),
                clipper: CustomCliper(),
              ),
              Align(
                child: Column(
                  children: [
                    Container(
                      width: SizeConfig.screenHeight * 0.15,
                      height: SizeConfig.screenHeight * 0.15,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: AssetImage('assets/images/profile.jpg'),
                          fit: BoxFit.cover,
                        ),
                        border: Border.all(width: 5, color: Colors.white),
                      ),
                    ),
                    Text('Joy Joy'),
                    Text('joyjoy@gamil.com')
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class CustomCliper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0, size.height - size.height * 0.5);
    path.quadraticBezierTo(size.width / 2, size.height, size.width,
        size.height - size.height * 0.5);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
