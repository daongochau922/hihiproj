import 'package:e_commerce/contants.dart';
import 'package:e_commerce/e_commerce/components/default_button.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class OtpScreen extends StatelessWidget {
  static String routeName = '/otp';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('OTP verification'),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: SizeConfig.screenHeight * 0.05,
          ),
          Text(
            'OTP Verification',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: getProportionateScreenWidth(30),
              color: Colors.black,
            ),
          ),
          Text(
            'We sent your code to +1 898 860 *** \nThis code will expired in 00:30',
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: SizeConfig.screenHeight * 0.15,
          ),
          OtpForm(),
          SizedBox(
            height: SizeConfig.screenHeight * 0.15,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: DefaultButton(
              text: 'Continue',
              onPressed: () {},
            ),
          ),
          SizedBox(
            height: SizeConfig.screenHeight * 0.1,
          ),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: 'Resend OTP Code',
              style: TextStyle(
                fontSize: getProportionateScreenWidth(18),
                color: kTextColor,
                decoration: TextDecoration.underline,
              ),
              recognizer: TapGestureRecognizer()..onTap = () {},
            ),
          )
        ],
      ),
    );
  }
}

class OtpForm extends StatefulWidget {
  const OtpForm({
    Key key,
  }) : super(key: key);

  @override
  _OtpFormState createState() => _OtpFormState();
}

class _OtpFormState extends State<OtpForm> {
  FocusNode pin2FocusNote;
  FocusNode pin3FocusNote;
  FocusNode pin4FocusNote;

  @override
  void initState() {
    super.initState();
    pin2FocusNote = FocusNode();
    pin3FocusNote = FocusNode();
    pin4FocusNote = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    pin2FocusNote.dispose();
    pin3FocusNote.dispose();
    pin4FocusNote.dispose();
  }

  void nextFocus({String value, FocusNode focusNode}) {
    if (value.length == 1) {
      focusNode.requestFocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        SizedBox(
          width: getProportionateScreenWidth(60),
          height: getProportionateScreenHeight(100),
          child: TextField(
            autofocus: true,
            decoration: otpInputDecoration,
            textAlign: TextAlign.center,
            obscureText: true,
            keyboardType: TextInputType.number,
            onChanged: (value) {
              nextFocus(value: value, focusNode: pin2FocusNote);
            },
          ),
        ),
        SizedBox(
          width: getProportionateScreenWidth(60),
          height: getProportionateScreenHeight(100),
          child: TextField(
            focusNode: pin2FocusNote,
            decoration: otpInputDecoration,
            textAlign: TextAlign.center,
            obscureText: true,
            keyboardType: TextInputType.number,
            onChanged: (value) {
              nextFocus(value: value, focusNode: pin3FocusNote);
            },
          ),
        ),
        SizedBox(
          width: getProportionateScreenWidth(60),
          height: getProportionateScreenHeight(100),
          child: TextField(
            focusNode: pin3FocusNote,
            decoration: otpInputDecoration,
            textAlign: TextAlign.center,
            obscureText: true,
            keyboardType: TextInputType.number,
            onChanged: (value) {
              nextFocus(value: value, focusNode: pin4FocusNote);
            },
          ),
        ),
        SizedBox(
          width: getProportionateScreenWidth(60),
          height: getProportionateScreenHeight(100),
          child: TextField(
            focusNode: pin4FocusNote,
            decoration: otpInputDecoration,
            textAlign: TextAlign.center,
            obscureText: true,
            keyboardType: TextInputType.number,
            onChanged: (value) {
              pin4FocusNote.unfocus();
            },
          ),
        ),
      ],
    );
  }
}
