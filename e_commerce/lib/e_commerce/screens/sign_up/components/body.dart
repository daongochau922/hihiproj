import 'package:e_commerce/e_commerce/components/socal_card.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

import 'sign_up_form.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Text(
          'Register Account',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: getProportionateScreenHeight(30),
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        Text(
          'Complete your detail or continue \nwith social media',
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: getProportionateScreenHeight(SizeConfig.screenHeight * 0.05),
        ),
        SignUpForm(),
        SizedBox(
          height: SizeConfig.screenHeight * 0.05,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SocalCard(image: 'assets/icons/google-icon.svg'),
            SocalCard(image: 'assets/icons/facebook-2.svg'),
            SocalCard(image: 'assets/icons/twitter.svg')
          ],
        ),
        SizedBox(
          height: SizeConfig.screenHeight * 0.02,
        ),
        Text(
          'By continuing your confirm that you agree \nwith our Term and Condition',
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
