import 'package:e_commerce/e_commerce/components/custom_suffix_icon.dart';
import 'package:e_commerce/e_commerce/components/default_button.dart';
import 'package:e_commerce/e_commerce/screens/complete_profile/complete_profile_screen.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class SignUpForm extends StatelessWidget {
  const SignUpForm({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(20),
        ),
        child: Column(
          children: [
            TextFormField(
              decoration: InputDecoration(
                  labelText: 'Email',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: 'Enter your email',
                  suffixIcon: CustomSuffixIcon(
                    svgIcon: 'assets/icons/Mail.svg',
                  )),
            ),
            SizedBox(
              height: SizeConfig.screenHeight * 0.02,
            ),
            TextFormField(
              decoration: InputDecoration(
                  labelText: 'Password',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: 'Enter your password',
                  suffixIcon: CustomSuffixIcon(
                    svgIcon: 'assets/icons/Lock.svg',
                  )),
            ),
            SizedBox(
              height: SizeConfig.screenHeight * 0.02,
            ),
            TextFormField(
              decoration: InputDecoration(
                  labelText: 'Confirm Password',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: 'Re-Enter your password',
                  suffixIcon: CustomSuffixIcon(
                    svgIcon: 'assets/icons/Lock.svg',
                  )),
            ),
            SizedBox(
              height: SizeConfig.screenHeight * 0.05,
            ),
            DefaultButton(
              text: 'Continue',
              onPressed: () {
                Navigator.pushNamed(context, ConpleteProfileScreen.routeName);
              },
            ),
          ],
        ),
      ),
    );
  }
}
