import 'package:e_commerce/contants.dart';
import 'package:e_commerce/e_commerce/components/default_button.dart';
import 'package:e_commerce/e_commerce/model/card.dart';
import 'package:e_commerce/e_commerce/screens/cart/component/body.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CartScreen extends StatelessWidget {
  static String routeName = '/cart';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
        title: Column(
          children: [
            Text(
              'Your Cart',
              style: TextStyle(
                fontSize: getProportionateScreenWidth(15),
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            Text('${demoCarts.length} items'),
          ],
        ),
      ),
      body: Body(),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(
          getProportionateScreenWidth(20),
        ),
        // height: 174,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -15),
              blurRadius: 20,
              color: Color(0xFFDADADA).withOpacity(0.30),
            ),
          ],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(getProportionateScreenWidth(7)),
                  width: getProportionateScreenWidth(36),
                  height: getProportionateScreenWidth(36),
                  decoration: BoxDecoration(
                    color: Color(0xFFF5F6F9),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: SvgPicture.asset('assets/icons/receipt.svg'),
                ),
                Spacer(),
                Text('Add voucher code'),
                Icon(
                  Icons.arrow_forward_ios,
                  color: kTextColor,
                  size: 14,
                ),
              ],
            ),
            SizedBox(
              height: getProportionateScreenWidth(20),
            ),
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Total:'),
                    Text('\$337.15'),
                  ],
                ),
                Spacer(),
                SizedBox(
                  width: SizeConfig.screenWidth * 0.5,
                  height: getProportionateScreenHeight(70),
                  child: DefaultButton(
                    text: 'Check Out',
                    onPressed: () {},
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
