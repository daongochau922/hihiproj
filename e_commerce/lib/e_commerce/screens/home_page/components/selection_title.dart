import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class SelectionTitle extends StatelessWidget {
  const SelectionTitle({
    Key key,
    this.text,
    this.press,
  }) : super(key: key);
  final String text;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          text,
          style: TextStyle(
            color: Colors.black,
            fontSize: getProportionateScreenWidth(16),
          ),
        ),
        GestureDetector(
          onTap: press,
          child: Text(
            'See more',
            style: TextStyle(
              color: Colors.black.withOpacity(0.3),
            ),
          ),
        ),
      ],
    );
  }
}
