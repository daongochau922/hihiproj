import 'package:e_commerce/contants.dart';
import 'package:e_commerce/e_commerce/screens/cart/cart_screen.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Container(
            height: 40,
            child: TextField(
              decoration: InputDecoration(
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                hintText: 'Search Product',
                prefixIcon: Icon(Icons.search),
                contentPadding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20),
                  vertical: getProportionateScreenHeight(4),
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: kSecondaryColor.withOpacity(0.1),
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        IconBthWithCounter(
          svgSrc: 'assets/icons/Cart Icon.svg',
          press: () {
            Navigator.pushNamed(context, CartScreen.routeName);
          },
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        IconBthWithCounter(
          svgSrc: 'assets/icons/Bell.svg',
          numOfItem: 12,
          press: () {},
        ),
      ],
    );
  }
}

class IconBthWithCounter extends StatelessWidget {
  const IconBthWithCounter({
    Key key,
    this.svgSrc,
    this.numOfItem,
    this.press,
  }) : super(key: key);
  final String svgSrc;
  final int numOfItem;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: press,
      borderRadius: BorderRadius.circular(50),
      child: Stack(
        overflow: Overflow.visible,
        children: [
          Container(
            height: 40,
            width: 40,
            padding: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(11),
              vertical: getProportionateScreenHeight(5),
            ),
            decoration: BoxDecoration(
              color: kSecondaryColor.withOpacity(0.1),
              borderRadius: BorderRadius.circular(25),
            ),
            child: SvgPicture.asset(svgSrc),
          ),
          if (numOfItem != null && numOfItem != 0)
            Positioned(
              top: -7,
              right: -5,
              child: Container(
                width: getProportionateScreenWidth(25),
                height: getProportionateScreenHeight(25),
                decoration: BoxDecoration(
                  color: Color(0xFFFF4848),
                  shape: BoxShape.circle,
                  border: Border.all(
                    width: 1.5,
                    color: Colors.white,
                  ),
                ),
                child: Center(
                  child: Text(
                    numOfItem.toString(),
                    style: TextStyle(
                      fontSize: 9,
                      color: Colors.white,
                      height: 1,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
