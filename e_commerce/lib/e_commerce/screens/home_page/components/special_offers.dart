import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

import 'selection_title.dart';

class SpecialOffer extends StatelessWidget {
  const SpecialOffer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SelectionTitle(
          text: 'Special for you',
          press: () {},
        ),
        SizedBox(height: getProportionateScreenHeight(5)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SpecialOfferCard(
                image: 'assets/images/Image Banner 2.png',
                category: 'Smartphone',
                numOfBrands: 18,
                onpress: () {},
              ),
              SpecialOfferCard(
                image: 'assets/images/Image Banner 3.png',
                category: 'Fashion',
                numOfBrands: 24,
                onpress: () {},
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class SpecialOfferCard extends StatelessWidget {
  const SpecialOfferCard({
    Key key,
    this.image,
    this.category,
    this.numOfBrands,
    this.onpress,
  }) : super(key: key);
  final String image;
  final String category;
  final int numOfBrands;
  final GestureTapCallback onpress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onpress,
      child: Padding(
        padding: EdgeInsets.only(right: 10),
        child: SizedBox(
          width: SizeConfig.screenWidth * 0.6,
          height: SizeConfig.screenHeight * 0.15,
          child: ClipRRect(
            child: Stack(
              children: [
                Image.asset(
                  image,
                  fit: BoxFit.fill,
                  width: SizeConfig.screenWidth * 0.6,
                  height: SizeConfig.screenHeight * 0.15,
                ),
                Container(
                  // width: double.infinity,
                  // height: 160,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xFF343434).withOpacity(0.4),
                        Color(0xFF343434).withOpacity(0.15),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(10),
                    vertical: getProportionateScreenHeight(15),
                  ),
                  child: RichText(
                    text: TextSpan(
                        text: category,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        ),
                        children: [
                          TextSpan(
                            text: '\n' + numOfBrands.toString() + ' bounds',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: getProportionateScreenHeight(16),
                            ),
                          ),
                        ]),
                  ),
                ),
              ],
            ),
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }
}
