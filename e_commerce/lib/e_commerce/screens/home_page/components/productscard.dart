import 'package:e_commerce/e_commerce/model/product.dart';
import 'package:e_commerce/e_commerce/screens/details/details_screen.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

import 'products.dart';
import 'selection_title.dart';

class ProductsCard extends StatelessWidget {
  const ProductsCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SelectionTitle(
          text: 'Popular Product',
          press: () {},
        ),
        SizedBox(
          height: getProportionateScreenHeight(5),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ...List.generate(
                demoProducts.length,
                (index) => ProductCard(
                  product: demoProducts[index],
                  onPress: () => Navigator.pushNamed(
                    context,
                    DetailsScreen.routeName,
                    arguments:
                        ProductDetailArguments(product: demoProducts[index]),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
