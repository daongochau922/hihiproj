import 'package:e_commerce/size_config.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class DiscountBanner extends StatelessWidget {
  const DiscountBanner({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      // splashColor: Colors.yellow,
      color: Color(0xFF4A3298),
      onPressed: () {},
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20),
        width: double.infinity,
        height: 80,
        child: Row(
          children: [
            RichText(
              text: TextSpan(
                text: 'A summer Surpise\n',
                children: [
                  TextSpan(
                    text: 'Cashback 20%',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: getProportionateScreenWidth(22),
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        Fluttertoast.showToast(
                            msg: 'hehe', toastLength: Toast.LENGTH_SHORT);
                      },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
