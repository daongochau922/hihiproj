import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

import 'components/categories.dart';
import 'components/discount_banner.dart';
import 'components/home_header.dart';
import 'components/productscard.dart';
import 'components/special_offers.dart';

class HomePageScreen extends StatelessWidget {
  static String routeName = '/Home';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              SizedBox(height: getProportionateScreenHeight(10)),
              HomeHeader(),
              SizedBox(height: getProportionateScreenHeight(10)),
              DiscountBanner(),
              SizedBox(height: getProportionateScreenHeight(10)),
              Categories(),
              SizedBox(height: getProportionateScreenHeight(10)),
              SpecialOffer(),
              SizedBox(height: getProportionateScreenHeight(10)),
              ProductsCard(),
            ],
          ),
        ),
      ),
    );
  }
}
