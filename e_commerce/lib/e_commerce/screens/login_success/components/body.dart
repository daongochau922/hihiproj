import 'package:e_commerce/contants.dart';
import 'package:e_commerce/e_commerce/components/default_button.dart';
import 'package:e_commerce/e_commerce/screens/home_page/home_page_screen.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Positioned(
            left: SizeConfig.screenWidth * 0.1,
            child: Image.asset(
              'assets/images/light-2.png',
              fit: BoxFit.cover,
              color: kPrimaryColor,
            ),
          ),
          Positioned(
            right: SizeConfig.screenWidth * 0.2,
            child: Image.asset(
              'assets/images/light-2.png',
              fit: BoxFit.cover,
              color: kPrimaryColor,
            ),
            height: SizeConfig.screenHeight * 0.2,
          ),
          Positioned(
            right: SizeConfig.screenWidth * 0.3,
            child: Image.asset(
              'assets/images/light-2.png',
              fit: BoxFit.cover,
              color: kPrimaryColor,
            ),
            height: SizeConfig.screenHeight * 0.15,
          ),
          Column(
            children: [
              Image.asset(
                'assets/images/success.png',
                height: SizeConfig.screenHeight * 0.5,
                width: SizeConfig.screenWidth,
              ),
              Spacer(),
              Text(
                'Login Success',
                style: TextStyle(
                  fontSize: getProportionateScreenWidth(25),
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 0.1 * SizeConfig.screenHeight,
              ),
              SizedBox(
                width: 0.6 * SizeConfig.screenWidth,
                child: DefaultButton(
                  text: 'Back to home',
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, HomePageScreen.routeName, (route) => false);
                  },
                ),
              ),
              Spacer(),
            ],
          ),
        ],
      ),
    );
  }
}
