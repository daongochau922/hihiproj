import 'package:e_commerce/contants.dart';
import 'package:e_commerce/e_commerce/components/socal_card.dart';
import 'package:e_commerce/e_commerce/screens/sign_up/sign_up_screen.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'sign_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
          ),
          child: ListView(
            children: [
              Text(
                'Welcome Back',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: getProportionateScreenWidth(28),
                ),
              ),
              Text(
                'Sign in with your email and password \nor continue with social media',
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: SizeConfig.screenHeight * 0.08,
              ),
              SignForm(),
              SizedBox(
                height: SizeConfig.screenHeight * 0.08,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SocalCard(
                    image: 'assets/icons/facebook-2.svg',
                  ),
                  SocalCard(
                    image: 'assets/icons/google-icon.svg',
                  ),
                  SocalCard(
                    image: 'assets/icons/twitter.svg',
                  ),
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(20),
              ),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Don't have an account?",
                  style: TextStyle(
                    color: kTextColor,
                  ),
                  children: [
                    TextSpan(
                      text: "Sign Up",
                      style: TextStyle(
                          color: kPrimaryColor,
                          decoration: TextDecoration.underline),
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.pushNamed(context, SignUpScreen.routeName);
                        },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
