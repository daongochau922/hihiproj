import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../size_config.dart';

class SocalCard extends StatelessWidget {
  const SocalCard({
    Key key,
    @required this.image,
    this.onPress,
  }) : super(key: key);
  final String image;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(
        getProportionateScreenWidth(6),
      ),
      height: getProportionateScreenHeight(40),
      width: getProportionateScreenWidth(40),
      decoration: BoxDecoration(
        color: Color(0xFFF5F6F9),
        shape: BoxShape.circle,
      ),
      child: SvgPicture.asset(image),
    );
  }
}
