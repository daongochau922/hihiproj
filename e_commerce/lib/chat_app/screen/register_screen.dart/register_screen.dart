import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e_commerce/chat_app/model/user.dart' as uuu;
import 'package:e_commerce/e_commerce/components/default_button.dart';
import 'package:e_commerce/size_config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart'; // For Image Picker

class RegisterChatScreen extends StatefulWidget {
  static String routeName = '/registerchatscreen';

  @override
  _RegisterChatScreenState createState() => _RegisterChatScreenState();
}

class _RegisterChatScreenState extends State<RegisterChatScreen> {
  File _image;
  final picker = ImagePicker();
  TextEditingController _emailCtl = TextEditingController();
  TextEditingController _passCtl = TextEditingController();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(
      () {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
        } else {
          print('No image selected.');
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  height: SizeConfig.screenHeight * 0.5,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/background.png'),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: getProportionateScreenHeight(40)),
                      TextField(
                        controller: _emailCtl,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText: 'Email',
                          hintText: 'Please input email',
                        ),
                      ),
                      SizedBox(height: getProportionateScreenHeight(20)),
                      TextField(
                        controller: _passCtl,
                        obscureText: true,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText: 'Password',
                          hintText: 'Please input password',
                        ),
                      ),
                      SizedBox(height: getProportionateScreenHeight(20)),
                      DefaultButton(
                        text: 'Register',
                        onPressed: () async {
                          String result =
                              await register(_emailCtl.text, _passCtl.text);

                          Fluttertoast.showToast(
                              msg: result == '' ? 'Register completed' : result,
                              toastLength: Toast.LENGTH_LONG);
                          if (result == '') {
                            Navigator.pop(context);
                          }
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              left: SizeConfig.screenWidth * 0.07,
              top: SizeConfig.screenWidth * 0.07,
              child: CircleAvatar(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: _image == null
                      ? Image.asset('assets/images/background.png')
                      : Image.file(
                          _image,
                          fit: BoxFit.cover,
                          width: double.infinity,
                          height: double.infinity,
                        ),
                ),
                radius: 80,
              ),
            ),
            Positioned(
              left: SizeConfig.screenWidth * 0.37,
              top: SizeConfig.screenWidth * 0.37,
              child: GestureDetector(
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(width: 2, color: Colors.white),
                  ),
                  child: CircleAvatar(
                    child: Icon(Icons.photo_camera),
                  ),
                ),
                onTap: () {
                  getImage();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<String> register(String strEmail, String strPass) async {
    FirebaseAuth fbAuth = FirebaseAuth.instance;
    try {
      await fbAuth
          .createUserWithEmailAndPassword(email: strEmail, password: strPass)
          .then(
        (userAuth) {
          registerPicture(_image, userAuth.user.uid).then((imageUrl) async {
            uuu.User user = new uuu.User(
              idUser: userAuth.user.uid,
              name: strEmail,
              urlAvatar: imageUrl,
              lastMessageTime: DateTime.now(),
            );
            await registerInfo(user);
          });
        },
      );
    } on FirebaseAuthException catch (e) {
      return e.code;
    }
    return '';
  }

  registerInfo(uuu.User user) async {
    await FirebaseFirestore.instance
        .collection('user')
        .doc(user.idUser)
        .set(user.tojson());
  }

  Future<String> registerPicture(File image, String uid) async {
    String filename =
        '/' + uid + image.path.substring(image.path.lastIndexOf('.'));
    Reference ref =
        FirebaseStorage.instance.ref().child('userimage').child(filename);
    await ref.putFile(image);

    return ref.fullPath;
  }
}
