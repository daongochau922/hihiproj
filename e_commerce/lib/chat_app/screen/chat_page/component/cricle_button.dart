import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class CricleButton extends StatelessWidget {
  const CricleButton({
    Key key,
    @required this.icon,
    @required this.onPress,
  }) : super(key: key);
  final IconData icon;
  final GestureTapCallback onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(5)),
        width: getProportionateScreenWidth(28),
        height: getProportionateScreenWidth(28),
        decoration: BoxDecoration(
          color: Colors.blue[100],
          shape: BoxShape.circle,
        ),
        child: Icon(
          icon,
          color: Colors.white,
        ),
      ),
    );
  }
}
