import 'package:e_commerce/chat_app/model/user.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

import 'component/cricle_button.dart';

class ChatScreen extends StatelessWidget {
  static String routeName = '/chatscreen';
  @override
  Widget build(BuildContext context) {
    final ChatAgruemnt arg = ModalRoute.of(context).settings.arguments;
    final user = arg.user;
    return Scaffold(
      backgroundColor: Colors.blue[400],
      appBar: AppBar(
        backgroundColor: Colors.blue[400],
        leading: FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
        title: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            user.name,
            style: TextStyle(
              color: Colors.white,
              fontSize: getProportionateScreenWidth(20),
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        actions: [
          CricleButton(
            icon: Icons.phone,
            onPress: () {},
          ),
          CricleButton(
            icon: Icons.videocam,
            onPress: () {},
          ),
          SizedBox(
            width: getProportionateScreenWidth(10),
          )
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
        ),
      ),
      bottomNavigationBar: InputMessage(),
    );
  }
}

class InputMessage extends StatelessWidget {
  const InputMessage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(getProportionateScreenWidth(4)),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
            child: TextField(
              autocorrect: true,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                hintText: 'Type your message',
              ),
              style: TextStyle(fontSize: getProportionateScreenWidth(16)),
            ),
          ),
          CricleButton(
            icon: Icons.send,
            onPress: () {},
          ),
        ],
      ),
    );
  }
}

class ChatAgruemnt {
  final User user;

  ChatAgruemnt({@required this.user});
}
