import 'package:e_commerce/chat_app/model/user.dart' as usera;
import 'package:e_commerce/chat_app/model/user_profile.dart';
import 'package:e_commerce/chat_app/screen/chats_page/chats_page.dart';
import 'package:e_commerce/chat_app/screen/forgot_password_chat.dart/forgot_pass.dart';
import 'package:e_commerce/chat_app/screen/register_screen.dart/register_screen.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

class SignInChatScreen extends StatefulWidget {
  static String routeName = '/signinchat';

  @override
  _SignInChatScreenState createState() => _SignInChatScreenState();
}

class _SignInChatScreenState extends State<SignInChatScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String _message = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: SizeConfig.screenHeight * 0.4,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage('assets/images/background.png'),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.screenWidth * 0.1),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        'Login',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: getProportionateScreenWidth(26),
                          color: Colors.black.withOpacity(0.8),
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                      hintText: 'Enter your email',
                      labelText: 'Email',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                    ),
                  ),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  TextField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                      hintText: 'Enter your password',
                      labelText: 'Password',
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                    ),
                  ),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  Text(
                    _message,
                  ),
                  GestureDetector(
                    child: Text('Forgot Password?'),
                    onTap: () => Navigator.pushNamed(
                        context, ForgotPasswordChatScreen.routeName),
                  ),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  Container(
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(20)),
                    width: SizeConfig.screenWidth * 0.4,
                    height: getProportionateScreenHeight(50),
                    child: FlatButton(
                      color: Colors.blue[200],
                      onPressed: () async {
                        // LoadingDialog.showLoadingDialog(context, 'Loading');
                        var message = await signWithEmail();
                        if (message != null) {
                          setState(() {
                            _message = message;
                          });
                        }
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(
                          color: Colors.black.withOpacity(0.7),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                  ),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  GestureDetector(
                    child: Text(
                      'Craete Account',
                      style: TextStyle(
                        color: Colors.blue[400],
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(
                          context, RegisterChatScreen.routeName);
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<String> signWithEmail() async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(
              email: emailController.text.trim(),
              password: passwordController.text);
      usera.User u = new usera.User(name: userCredential.user.email);
      Provider.of<UserProfile>(context, listen: false).setUser(u);
      Navigator.pushNamed(context, MainChatApp.routeName);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        return 'Wrong password provided for that user.';
      }
    }
    return null;
  }
}
