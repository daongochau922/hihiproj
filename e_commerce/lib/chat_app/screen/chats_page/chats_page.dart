import 'package:e_commerce/chat_app/api/firebase_api.dart';
import 'package:e_commerce/chat_app/model/user.dart';
import 'package:e_commerce/chat_app/model/user_profile.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'component/chat_header.dart';
import 'component/chat_list.dart';

class MainChatApp extends StatefulWidget {
  static String routeName = '/mainchatapp';

  @override
  _MainChatAppState createState() => _MainChatAppState();
}

class _MainChatAppState extends State<MainChatApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[200],
      appBar: AppBar(
        leading: FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
        backgroundColor: Colors.blue[200],
        title: Text(
          Provider.of<UserProfile>(context).getUser.name,
          style: TextStyle(
            color: Colors.white,
            fontSize: getProportionateScreenWidth(24),
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: StreamBuilder<List<User>>(
        stream: FirebaseApi.getUsers(),
        builder: (context, snapshot) {
          List<User> _list = snapshot.data;
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            default:
              if (snapshot.hasError) {
                return Center(
                  child: Text('Something Wrong'),
                );
              } else {
                return Column(
                  children: [
                    ChatsHeader(
                      list: _list,
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(20),
                    ),
                    ChatsList(
                      list: _list,
                    ),
                  ],
                );
              }
          }
        },
      ),
      floatingActionButton: FloatingActionButton(onPressed: () {
        User u = new User(name: 'heheheheh');
        Provider.of<UserProfile>(context, listen: false).setUser(u);
      }),
    );
  }
}
