import 'package:e_commerce/chat_app/model/user.dart';
import 'package:e_commerce/chat_app/screen/chat_page/chat_screen.dart';
import 'package:flutter/material.dart';

import '../../../../size_config.dart';

class ChatsList extends StatelessWidget {
  const ChatsList({
    Key key,
    @required List<User> list,
  })  : _list = list,
        super(key: key);

  final List<User> _list;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
        ),
        child: ListView.builder(
          itemBuilder: (context, index) => GestureDetector(
            onTap: () => Navigator.pushNamed(context, ChatScreen.routeName,
                arguments: ChatAgruemnt(user: _list[index])),
            child: Container(
              padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
              child: Row(
                children: [
                  CircleAvatar(
                    backgroundImage: NetworkImage(_list[index].urlAvatar),
                    radius: getProportionateScreenWidth(24),
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(20),
                  ),
                  Text(
                    _list[index].name,
                    style: TextStyle(fontSize: getProportionateScreenWidth(16)),
                  ),
                ],
              ),
            ),
          ),
          itemCount: _list.length,
        ),
      ),
    );
  }
}
