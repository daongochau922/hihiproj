import 'package:e_commerce/chat_app/model/user.dart';
import 'package:e_commerce/chat_app/screen/chat_page/chat_screen.dart';
import 'package:e_commerce/size_config.dart';
import 'package:flutter/material.dart';

class ChatsHeader extends StatelessWidget {
  const ChatsHeader({
    Key key,
    @required List<User> list,
  })  : _list = list,
        super(key: key);

  final List<User> _list;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(10),
      ),
      width: double.infinity,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            CircleAvatar(
              child: Icon(Icons.search),
              backgroundColor: Colors.red[400],
              radius: getProportionateScreenHeight(30),
            ),
            ...List.generate(
              _list.length,
              (index) => GestureDetector(
                onTap: () => Navigator.pushNamed(context, ChatScreen.routeName,
                    arguments: ChatAgruemnt(user: _list[index])),
                child: Container(
                  margin: EdgeInsets.only(
                    left: getProportionateScreenHeight(10),
                  ),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(_list[index].urlAvatar),
                    radius: getProportionateScreenHeight(30),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
