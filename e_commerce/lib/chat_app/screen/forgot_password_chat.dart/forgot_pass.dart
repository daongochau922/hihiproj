import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class ForgotPasswordChatScreen extends StatefulWidget {
  static String routeName = '/forgotpasswordchatscreen';

  @override
  _ForgotPasswordChatScreenState createState() =>
      _ForgotPasswordChatScreenState();
}

class _ForgotPasswordChatScreenState extends State<ForgotPasswordChatScreen> {
  var list = [
    'adasdadadadadasdasdadasdasdasdasdas',
    'adadasdasdasdasdasdfasdasd',
    'adasdasfjksksjfhjasfjhahsf',
    'adadasdasdasdasdasdfasdasdadadasdasdasdasdasdfasdasdadadasdasdasdasdasdfasdasdadadasdasdasdasdasdfasdasd',
    'adadasdasdasdasdasdfasdasd',
    'adadasdasdasdasdasdfasdasd',
    'adasdadadadadasdasdadasdasdasdasdas',
    'adadasdasdasdasdasdfasdasd',
    'adasdasfjksksjfhjasfjhahsf',
    'adadasdasdasdasdasdfasdasdadadasdasdasdasdasdfasdasdadadasdasdasdasdasdfasdasdadadasdasdasdasdasdfasdasd',
    'adadasdasdasdasdasdfasdasd',
    'adadasdasdasdasdasdfasdasd',
  ];

  int column = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Forgot passowrd'),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        width: double.infinity,
        height: double.infinity,
        child: StaggeredGridView.countBuilder(
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          itemCount: list.length,
          crossAxisCount: 2,
          itemBuilder: (context, index) => Container(
            child: Text(
              list[index],
            ),
            decoration: BoxDecoration(
              color: Colors.blue[200],
            ),
          ),
          staggeredTileBuilder: (index) => StaggeredTile.fit(column),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (column == 1) {
              column = 2;
            } else {
              column = 1;
            }
          });
        },
      ),
    );
  }
}
