import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:e_commerce/chat_app/model/user.dart';

class FirebaseApi {
  static Stream<List<User>> getUsers() => FirebaseFirestore.instance
      .collection('user')
      .orderBy(UserField.lastMessageTime, descending: true)
      .snapshots()
      .transform(User.transformer(User.fromJson));
}
