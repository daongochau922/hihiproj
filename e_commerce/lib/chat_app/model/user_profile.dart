import 'package:e_commerce/chat_app/model/user.dart';
import 'package:flutter/foundation.dart';

class UserProfile with ChangeNotifier {
  User user;

  User get getUser => user;

  void setUser(User _user) {
    user = _user;
    notifyListeners();
  }
}
