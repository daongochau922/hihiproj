import 'package:e_commerce/animation/main_animation.dart';
import 'package:e_commerce/chat_app/screen/sign_in/signin_chat_screen.dart';
import 'package:e_commerce/e_commerce/screens/splash/splash_screen.dart';
import 'package:flutter/material.dart';

import 'drag_drop_game/main_drag.dart';
import 'size_config.dart';

class Menu extends StatelessWidget {
  static String routeName = '/menu';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Menu'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FlatButton(
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SplashScreen(),
                  )),
              child: Text('E-commerce'),
            ),
            FlatButton(
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MainDragDrop(),
                  )),
              child: Text('Drag and Drop game'),
            ),
            FlatButton(
              onPressed: () =>
                  Navigator.pushNamed(context, AnimationScreen.routeName),
              child: Text('Animation'),
            ),
            FlatButton(
              onPressed: () =>
                  Navigator.pushNamed(context, SignInChatScreen.routeName),
              child: Text('Chat app'),
            ),
          ],
        ),
      ),
    );
  }
}
