import 'package:flutter/material.dart';

class MainDragDrop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Drag and Drop demo'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Draggable(
            child: Icon(Icons.account_box),
            feedback: Icon(Icons.access_alarm),
            childWhenDragging: Icon(Icons.add),
            data: 'Knight',
            onDragCompleted: () {
              print('drag complete');
            },
            onDragEnd: (details) {
              print('drag end');
            },
            onDragStarted: () {
              print('drag started');
            },
            onDraggableCanceled: (velocity, offset) {
              print('drag cenceled');
            },
          ),
          Draggable(
            child: Icon(Icons.account_box),
            feedback: Icon(Icons.access_alarm),
            childWhenDragging: Icon(Icons.add),
            data: 'King',
            onDragCompleted: () {
              print('drag complete');
            },
            onDragEnd: (details) {
              print('drag end');
            },
            onDragStarted: () {
              print('drag started');
            },
            onDraggableCanceled: (velocity, offset) {
              print('drag cenceled');
            },
          ),
          DragTarget(
            builder: (context, List<String> candidateData, rejectedData) {
              print(candidateData.toString() + '  ' + rejectedData.toString());
              return Container(
                width: 100,
                height: 100,
                color: Colors.blue,
              );
            },
            onAccept: (data) {
              print('accept' + data.toString());
            },
            onAcceptWithDetails: (details) {
              print('acceptWithDetail');
            },
            onLeave: (data) {
              print('leave' + data.toString());
            },
            onWillAccept: (data) {
              print('will accept' + data.toString());
              return data == 'Knight' ? true : false;
            },
          )
        ],
      ),
    );
  }
}
