import 'dart:async';

import 'package:taxi/FIREBASE/firebase_auth.dart';

class LoginBloc {
  var _emailStreamCTL = StreamController();
  var _passwordStreamCTL = StreamController();

  Stream get emailStream => _emailStreamCTL.stream;
  Stream get passwordStream {
    return _passwordStreamCTL.stream;
  }

  bool isvalid(String email, String password) {
    var _hasErr = false;
    if (email == null || email.length == 0) {
      _emailStreamCTL.sink.addError('Please input Email!');
      _hasErr = true;
    } else {
      _emailStreamCTL.sink.add('');
    }

    if (password == null || password.length == 0) {
      _passwordStreamCTL.sink.addError('Please input password!');
      _hasErr = true;
    } else {
      _passwordStreamCTL.sink.add('');
    }

    return _hasErr;
  }

  var fbauth = FirAuth();
  void signIn(String email, String password, Function onSuccess,
      Function(String) onSignInError) {
    fbauth.signIn(email, password, onSuccess, onSignInError);
  }

  void dispose() {
    _emailStreamCTL.close();
    _passwordStreamCTL.close();
  }
}
