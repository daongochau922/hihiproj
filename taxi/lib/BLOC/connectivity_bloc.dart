import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:taxi/Model/Enum.dart';

class ConnectivityBloc {
  StreamController<ConnectivityStatus> connectivityController =
      StreamController<ConnectivityStatus>();

  ConnectivityBloc() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      connectivityController.add(_getStatusFromResult(result));
    });
  }
  ConnectivityStatus _getStatusFromResult(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.mobile:
        return ConnectivityStatus.Cellular;
      case ConnectivityResult.wifi:
        return ConnectivityStatus.WiFi;
      case ConnectivityResult.none:
        return ConnectivityStatus.Offline;
      default:
        return ConnectivityStatus.Offline;
    }
  }
}
