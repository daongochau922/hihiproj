import 'dart:async';

import 'package:taxi/FIREBASE/firebase_auth.dart';

class Registerbloc {
  var _emailStreamCTL = StreamController();
  var _passwordStreamCTL = StreamController();
  var _nameStreamCTL = StreamController();
  var _phoneStreamCTL = StreamController();

  Stream get emailStream => _emailStreamCTL.stream;
  Stream get passwordStream => _passwordStreamCTL.stream;
  Stream get nameStream => _nameStreamCTL.stream;
  Stream get phoneStream => _phoneStreamCTL.stream;

  bool isCheckRegister(
      String email, String password, String name, String phone) {
    var _hasError = false;
    if (email == null || email.length == 0) {
      _emailStreamCTL.sink.addError('Please input email!');
      _hasError = true;
    } else {
      _emailStreamCTL.sink.add('');
    }

    if (password == null || password.length == 0) {
      _passwordStreamCTL.sink.addError('Please input password!');
      _hasError = true;
    } else {
      _passwordStreamCTL.sink.add('');
    }

    if (name == null || name.length == 0) {
      _nameStreamCTL.sink.addError('Please input name!');
      _hasError = true;
    } else {
      _nameStreamCTL.sink.add('');
    }

    if (phone == null || phone.length == 0) {
      _phoneStreamCTL.sink.addError('Please input phone!');
      _hasError = true;
    } else {
      _phoneStreamCTL.sink.add('');
    }
    return _hasError;
  }

  var fbauth = FirAuth();
  void signUp(String email, String password, String name, String phone,
      Function onSuccess, Function(String) onRegisterError) {
    fbauth.signUp(email, password, name, phone, onSuccess, onRegisterError);
  }

  void dispose() {
    _emailStreamCTL.close();
    _nameStreamCTL.close();
    _passwordStreamCTL.close();
    _phoneStreamCTL.close();
  }
}
