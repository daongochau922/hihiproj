import 'dart:async';

import 'package:taxi/REPOSITORY/PlaceService.dart';

class PlaceBloc {
  var _placeController = StreamController();
  Stream get placeStream => _placeController.stream;

  void searchPlace(String keyword) {
    _placeController.sink.add('start');
    PlaceService.searchPlace(keyword).then((value) {
      _placeController.sink.add(value);
    });
  }

  void dispose() {
    _placeController.close();
  }
}
