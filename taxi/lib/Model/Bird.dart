import 'package:flutter/material.dart';

class Bird with ChangeNotifier {
  Bird();
  Bird.fromPlayer(this.qty) {
    Bird b = new Bird();
    b.qty = this.qty;
  }
  int qty = 0;

  increment() {
    qty++;
    notifyListeners();
  }
}
