import 'package:google_maps_flutter/google_maps_flutter.dart';

class PlaceItem {
  String id;
  String title;
  String url;
  LatLng lag;

  PlaceItem(this.id, this.title, this.url, this.lag);

  static List<PlaceItem> fromJson(dynamic json) {
    List<PlaceItem> list = new List();
    for (var item in json['results']) {
      PlaceItem p = new PlaceItem(
          item['name'],
          item['name'],
          item['icon'],
          new LatLng(item['geometry']['location']['lat'],
              item['geometry']['location']['lng']));
      list.add(p);
    }
    return list;
  }
}
