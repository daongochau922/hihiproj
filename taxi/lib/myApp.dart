import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:taxi/BLOC/connectivity_bloc.dart';
import 'package:taxi/UI/HomePage.dart';
import 'package:taxi/UI/LoginPage.dart';
import 'package:flutter/services.dart';
import 'package:taxi/UI/HistoryPage.dart';
import 'package:taxi/UI/MyProfilePage.dart';

import 'Model/Enum.dart';
import 'Model/Bird.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  User user = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    print('aaaaaaaa');
    SystemChrome.setEnabledSystemUIOverlays(
        [SystemUiOverlay.top, SystemUiOverlay.bottom]);

    return StreamProvider<ConnectivityStatus>(
      create: (context) => ConnectivityBloc().connectivityController.stream,
      builder: (context, child) => MultiProvider(
        providers: [
          Provider<Bird>(
            create: (context) => Bird(),
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Taxi App',
          initialRoute: user == null ? '/' : '/HomePage',
          routes: {
            '/': (context) => LoginPage(),
            '/HomePage': (context) => HomePage(),
            '/HistoryPage': (context) => HistoryPage(),
            '/ProfilePage': (context) => MyProfilePage(),
            '/CounterWidget': (context) => CounterWidget(),
          },
        ),
      ),
    );

    // return MaterialApp(
    //   debugShowCheckedModeBanner: false,
    //   home: LoginPage(),
    // );
  }
}
