import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

class FirAuth {
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  void signUp(String email, String password, String name, String phone,
      Function onSuccess, Function(String) onRigisterError) {
    try {
      _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) => () {
                _createUser(
                    value.user.uid, name, phone, onSuccess, onRigisterError);
              })
          .catchError((onError) {
        print('iiiiiii');
        print(onError.code);
        _onSignUpErr(onError.code, onRigisterError);
      });
    } catch (e) {
      print('jjjjjj');
      print(e);
    }
  }

  _createUser(String userId, String name, String phone, Function onSuccess,
      Function(String) onRegisterError) {
    var user = {'name': name, 'phone': phone};
    var ref = FirebaseDatabase.instance.reference().child('user');
    ref.child(userId).set(user).then((value) {
      onSuccess();
    }).catchError((err) {
      onRegisterError("SignUp fail, Please try again");
    });
  }

  void signIn(String email, String password, Function onSuccess,
      Function(String) onSignInError) {
    _firebaseAuth
        .signInWithEmailAndPassword(email: email, password: password)
        .then((value) {
      onSuccess();
    }).catchError((onError) {
      print('tttttt');
      print(onError.code);
      _onSignInErr(onError.code, onSignInError);
    });
  }

  void getProfile() {
    DatabaseReference query = FirebaseDatabase.instance
        .reference()
        .child('user')
        .child('7GG8J1xeJwQ3vZBmNTCjdxW6qp93');
    query.onValue.listen((event) {
      print(event.snapshot.value);
    });
  }

  void updateProfile() {}

  void _onSignUpErr(String code, Function(String) onRegisterError) {
    switch (code) {
      case 'email-already-in-use':
        onRegisterError('Email already in use');
        break;
      case 'invalid-email':
        onRegisterError('Invalid email');
        break;
      case 'operation-not-allowed':
        onRegisterError('Operation not allowed');
        break;
      case 'weak-password':
        onRegisterError('Weak password');
        break;
      case 'firebase_auth':
        onRegisterError('Invalid mail');
        break;
      case 'user-not-found':
        onRegisterError('User not found');
        break;
      default:
        onRegisterError('Error with user');
        break;
    }
  }

  void _onSignInErr(String code, Function(String) onSignInError) {
    switch (code) {
      case 'email-already-in-use':
        onSignInError('Email already in use');
        break;
      case 'invalid-email':
        onSignInError('Invalid email');
        break;
      case 'operation-not-allowed':
        onSignInError('Operation not allowed');
        break;
      case 'weak-password':
        onSignInError('Weak password');
        break;
      case 'firebase_auth':
        onSignInError('Invalid mail');
        break;
      case 'user-not-found':
        onSignInError('User not found');
        break;
      case 'wrong-password':
        onSignInError('Wrong password');
        break;
      default:
        onSignInError('Error with user');
        break;
    }
  }
}
