import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:taxi/BLOC/login_bloc.dart';
import 'package:taxi/FIREBASE/firebase_auth.dart';
import 'package:taxi/UI/DIALOG/LoadingDialog.dart';
import 'package:taxi/UI/DIALOG/MsgDialog.dart';
import 'package:taxi/UI/RegisterPage.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  FirAuth fireAuth;
  LoginBloc loginBloc;

  @override
  void initState() {
    fireAuth = FirAuth();
    loginBloc = LoginBloc();
    super.initState();
  }

  @override
  void dispose() {
    loginBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 30),
              ),
              Image.asset(
                'car1.png',
                width: 150,
                height: 120,
              ),
              Text(
                'Welcome back!',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff333333)),
              ),
              Text(
                'Login to continue using iCab',
                style: TextStyle(
                    color: Color(0xff604070),
                    fontWeight: FontWeight.w100,
                    fontSize: 12),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 80, 50, 20),
                child: StreamBuilder(
                  stream: loginBloc.emailStream,
                  builder: (context, snapshot) => TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                      errorText: snapshot.hasError ? snapshot.error : null,
                      contentPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      labelText: 'Email',
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffCE00D2), width: 0.5),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 0, 50, 10),
                child: StreamBuilder(
                  stream: loginBloc.passwordStream,
                  builder: (context, snapshot) => TextField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                      errorText: snapshot.hasError ? snapshot.error : null,
                      contentPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      labelText: 'Password',
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1,
                          color: Color(0xffCE00D2),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(0, 0, 50, 0),
                alignment: Alignment.centerRight,
                child: Text(
                  'Forgot Password?',
                  style: TextStyle(color: Color(0xff606470), fontSize: 12),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 10, 50, 30),
                child: SizedBox(
                  width: double.infinity,
                  child: RaisedButton(
                    color: Color(0xff3277DB),
                    onPressed: () {
                      onSignInClick();
                    },
                    child: Text(
                      'Log In',
                      style: TextStyle(color: Color(0xfffafafa)),
                    ),
                  ),
                ),
              ),
              RichText(
                text: TextSpan(
                    text: 'New user?',
                    style: TextStyle(color: Color(0xff606470)),
                    children: [
                      TextSpan(
                        text: ' Sign up for a new account',
                        style: TextStyle(color: Color(0xff3277DB)),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => RegisterPage(),
                                ));
                          },
                      ),
                    ]),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onSignInClick() {
    if (loginBloc.isvalid(emailController.text, passwordController.text) ==
        false) {
      LoadingDialog.showLoadingDialog(context, 'Loading...');
      loginBloc.signIn(
          emailController.text.trim(), passwordController.text.trim(), () {
        print('home page');
        LoadingDialog.hideLoadingDialog(context);
        Navigator.pushNamedAndRemoveUntil(
            context, '/HomePage', (route) => false);
      }, (msg) {
        LoadingDialog.hideLoadingDialog(context);
        print('abcd' + msg);
        MsgDialog.showMsgDialog(context, 'Sign-In', msg);
      });
    } else {}
  }
}
