import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:taxi/Model/Enum.dart';
import 'package:taxi/Model/PlaceItem.dart';
import 'package:taxi/UI/WIDGET/HomeMenu.dart';
import 'package:taxi/UI/WIDGET/RidePicker.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isDropDownShow = false;
  OverlayEntry _overlayEntry;
  Map<MarkerId, Marker> markers = {};
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  GoogleMapController _mapController;
  final Map<String, Marker> _markers = <String, Marker>{};
  PlaceItem fromPlace, toPlace;
  Location location = Location();
  Marker _markerA;
  Circle _circleA;
  StreamSubscription _locationSubscription;

  @override
  Widget build(BuildContext context) {
    var connectivityStatus = Provider.of<ConnectivityStatus>(context);

    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: HomeMenu(),
      ),
      body: GestureDetector(
        onTap: () {
          setState(() {
            if (isDropDownShow) {
              _overlayEntry.remove();
            }
            isDropDownShow = !isDropDownShow;
          });
        },
        child: Container(
          child: Stack(
            children: [
              GoogleMap(
                //mapType: MapType.hybrid,
                onMapCreated: (controller) {
                  _mapController = controller;
                },
                onTap: (argument) {},
                polylines: Set.of(polylines.values),
                markers: Set.of(_markers.values),
                circles: Set.of((_circleA != null) ? [_circleA] : []),
                initialCameraPosition: CameraPosition(
                    target: LatLng(37.42796133580664, -122.085749655962),
                    zoom: 14.4746),
              ),
              Positioned(
                left: 0,
                top: 0,
                right: 0,
                child: Column(
                  children: [
                    AppBar(
                      backgroundColor: Colors.transparent,
                      elevation: 0,
                      leading: FlatButton(
                        onPressed: () {
                          _scaffoldKey.currentState.openDrawer();
                        },
                        child: Icon(Icons.menu),
                      ),
                      title: Center(
                        child: Text(
                          connectivityStatus.toString(),
                          style: TextStyle(
                            fontSize: 12,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                      actions: [
                        FlatButton(
                          focusColor: Colors.white,
                          hoverColor: Colors.white,
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onPressed: () {
                            setState(() {
                              if (isDropDownShow) {
                                _overlayEntry.remove();
                              } else {
                                _overlayEntry = createOverlay(context);
                                Overlay.of(context).insert(_overlayEntry);
                              }

                              isDropDownShow = !isDropDownShow;
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            child: Stack(
                              children: [
                                Center(
                                  child: Icon(Icons.notifications_none),
                                ),
                                Positioned(
                                  child: CircleAvatar(
                                    maxRadius: 6,
                                    child: Text(
                                      '10',
                                      style: TextStyle(fontSize: 8),
                                    ),
                                  ),
                                  left: 10,
                                  right: 0,
                                  bottom: 25,
                                  top: 10,
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    RidePicker(onPlaceSelected),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Navigator.of(context).push(MaterialPageRoute(
          //   builder: (context) => OverlayTest(),
          // ));
          getCurrentLocation();
        },
        child: Icon(
          Icons.location_searching,
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
    );
  }

  Future<Uint8List> getMarker() async {
    ByteData byteData =
        await DefaultAssetBundle.of(context).load("assets/car_icon.png");
    return byteData.buffer.asUint8List();
  }

  void updateMarkerAndCricle(LocationData newLocation, Uint8List image) {
    LatLng latLng = LatLng(newLocation.latitude, newLocation.longitude);
    this.setState(() {
      _markerA = new Marker(
        markerId: MarkerId('Home'),
        position: latLng,
        icon: BitmapDescriptor.fromBytes(image),
        rotation: newLocation.heading,
        anchor: Offset(0.5, 0.5),
        zIndex: 2,
      );
      _circleA = new Circle(
          circleId: CircleId('car'),
          radius: newLocation.accuracy,
          center: latLng,
          zIndex: 1,
          strokeColor: Colors.blue,
          fillColor: Colors.blue.withAlpha(70));

      markers.addAll({MarkerId('Home'): _markerA});
      _markers.addAll({'Home': _markerA});
      _mapController.animateCamera(CameraUpdate.newLatLng(latLng));
    });
  }

  void getCurrentLocation() async {
    try {
      Uint8List imagedata = await getMarker();
      LocationData locationData = await location.getLocation();

      updateMarkerAndCricle(locationData, imagedata);

      if (_locationSubscription != null) {
        _locationSubscription.cancel();
      }

      _locationSubscription = location.onLocationChanged.listen((newLocalData) {
        if (_mapController != null) {
          _mapController.animateCamera(
            CameraUpdate.newCameraPosition(
              new CameraPosition(
                // bearing: 192.8334901395799,
                target: LatLng(newLocalData.latitude, newLocalData.longitude),
                // tilt: 0,
                // zoom: 18.00,
              ),
            ),
          );
          updateMarkerAndCricle(newLocalData, imagedata);
        }
      });
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint("Permission Denied");
      }
    }
  }

  void onPlaceSelected(PlaceItem placeItem, bool fromAddress) {
    if (fromAddress) {
      fromPlace = placeItem;
    } else {
      toPlace = placeItem;
    }
    _mapController.animateCamera(CameraUpdate.newLatLng(placeItem.lag));
    _addMarker(placeItem.lag, 'From');
    if (fromPlace != null && toPlace != null) {
      _getPolyline(fromPlace.lag, toPlace.lag);
    }
    setState(() {});
  }

  OverlayEntry createOverlay(BuildContext context) {
    return OverlayEntry(
      builder: (context) {
        return Stack(
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  if (isDropDownShow) {
                    _overlayEntry.remove();
                    print('object');
                  }
                  isDropDownShow = !isDropDownShow;
                });
              },
              child: Container(
                height: double.infinity,
                width: double.infinity,
                color: Colors.transparent,
              ),
            ),
            Positioned(
              left: 60,
              width: 300,
              top: 60,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 5,
                  ),
                  Align(
                    alignment: Alignment(0.94, 0),
                    child: ClipPath(
                      clipper: ArrowClipper(),
                      child: Container(
                        height: 20,
                        width: 30,
                        decoration: BoxDecoration(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                  Material(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.all(
                          Radius.circular(12),
                        ),
                      ),
                      height: 450,
                      child: ListView(
                        children: [
                          ListTile(
                            title: Text('hahahahahahaha'),
                            subtitle: Text('hee'),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  // _moveCamera(LatLng point) {
  //   CameraUpdate cam = CameraUpdate.newLatLng(point);
  // }

  _addMarker(LatLng position, String id) {
    MarkerId markerId = MarkerId(id);
    Marker marker = Marker(markerId: markerId, position: position);
    markers[markerId] = marker;

    _markers.addAll({id: marker});
  }

  _addPolyLine() {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
        polylineId: id, color: Colors.red, points: polylineCoordinates);
    polylines[id] = polyline;
    setState(() {});
  }

  _getPolyline(LatLng from, LatLng to) async {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      'AIzaSyAkX6AffDXZMiu73y2GG2mVsrbx9gknvrM',
      PointLatLng(from.latitude, from.longitude),
      PointLatLng(to.latitude, to.longitude),
      travelMode: TravelMode.driving,
      // wayPoints: [PolylineWayPoint(location: "Sabo, Yaba Lagos Nigeria")],
    );
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    _addPolyLine();
  }
}

class ArrowClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.moveTo(0, size.height);
    path.lineTo(size.width / 2, 0);
    path.lineTo(size.width, size.height);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
