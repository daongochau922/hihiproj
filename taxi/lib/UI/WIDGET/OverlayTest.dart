import 'package:flutter/material.dart';
import 'package:overlay_container/overlay_container.dart';

class OverlayTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Overlay Container Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: OverlayTestApp(),
    );
  }
}

class OverlayTestApp extends StatefulWidget {
  _OverlayTestAppState createState() => _OverlayTestAppState();
}

class _OverlayTestAppState extends State<OverlayTestApp> {
  // Need to maintain a "show" state either locally or inside
  // a bloc.
  bool _dropdownShown = false;

  void _toggleDropdown() {
    setState(() {
      _dropdownShown = !_dropdownShown;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Overlay Container Demo Page'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            RaisedButton(
              onPressed: _toggleDropdown,
              child: Column(
                children: <Widget>[
                  Text("Dropdown button"),
                ],
              ),
            ),
            // By default the overlay (since this is a Column) will
            // be added right below the raised button
            // but outside the widget tree.
            // We can change that by supplying a "position".
            OverlayContainer(
              show: _dropdownShown,
              // Let's position this overlay to the right of the button.
              position: OverlayContainerPosition(
                // Left position.
                150,
                // Bottom position.
                45,
              ),
              // The content inside the overlay.
              child: Container(
                height: 70,
                padding: const EdgeInsets.all(20),
                margin: const EdgeInsets.only(top: 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.grey[300],
                      blurRadius: 3,
                      spreadRadius: 6,
                    )
                  ],
                ),
                child: Text("I render outside the \nwidget hierarchy."),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
