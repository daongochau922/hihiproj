import 'package:flutter/material.dart';
import 'package:taxi/Model/PlaceItem.dart';

import '../RidePickerPage.dart';

class RidePicker extends StatefulWidget {
  final Function(PlaceItem, bool) onSelected;

  RidePicker(this.onSelected);

  @override
  _RidePickerState createState() => _RidePickerState();
}

class _RidePickerState extends State<RidePicker> {
  PlaceItem fromPlace;
  PlaceItem toPlace;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                color: Color(0x88999999), offset: Offset(0, 5), blurRadius: 5.0)
          ]),
      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Column(
        children: [
          Container(
            child: FlatButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) =>
                        RidePickerPage(fromPlace == null ? '' : fromPlace.title,
                            (place, isFrom) {
                      widget.onSelected(place, isFrom);
                      fromPlace = place;
                      setState(() {});
                    }, true),
                  ),
                );
              },
              child: Row(
                children: [
                  Icon(Icons.location_on),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  ),
                  Expanded(
                    // child: TextField(
                    //   decoration: InputDecoration(
                    //     border: OutlineInputBorder(
                    //         borderRadius: BorderRadius.zero,
                    //         borderSide: BorderSide.none),
                    //     focusedBorder: InputBorder.none,
                    //     disabledBorder: InputBorder.none,
                    //     enabledBorder: InputBorder.none,
                    //     errorBorder: InputBorder.none,
                    //     focusedErrorBorder: InputBorder.none,
                    //   ),
                    // ),
                    child: Text(fromPlace == null ? 'From' : fromPlace.title),
                  ),
                  Icon(Icons.close),
                ],
              ),
            ),
          ),
          Container(
            child: FlatButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => RidePickerPage(
                        toPlace == null ? '' : toPlace.title, (place, isFrom) {
                      widget.onSelected(place, isFrom);
                      toPlace = place;
                      setState(() {});
                    }, false),
                  ),
                );
              },
              child: Row(
                children: [
                  Icon(Icons.send),
                  Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  ),
                  Expanded(
                    // child: TextField(
                    //   decoration: InputDecoration(
                    //     border: OutlineInputBorder(
                    //         borderRadius: BorderRadius.zero,
                    //         borderSide: BorderSide.none),
                    //     focusedBorder: InputBorder.none,
                    //     disabledBorder: InputBorder.none,
                    //     enabledBorder: InputBorder.none,
                    //     errorBorder: InputBorder.none,
                    //     focusedErrorBorder: InputBorder.none,
                    //   ),
                    // ),
                    child: Text(toPlace == null ? 'To' : toPlace.title),
                  ),
                  Icon(Icons.close),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
