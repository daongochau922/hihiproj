import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:taxi/UI/HistoryPage.dart';
import 'package:taxi/UI/MyProfilePage.dart';

class HomeMenu extends StatefulWidget {
  @override
  _HomeMenuState createState() => _HomeMenuState();
}

class _HomeMenuState extends State<HomeMenu> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        DrawerHeader(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0)),
              CircleAvatar(
                backgroundColor: Colors.yellow,
                backgroundImage: AssetImage('car1.png'),
                radius: 30,
              ),
              Padding(padding: EdgeInsets.fromLTRB(0, 15, 0, 0)),
              Text(
                'Nguyen Van Teo',
                style: TextStyle(fontSize: 20),
              ),
              Text(
                'Nguyen Van Teo',
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
            ],
          ),
        ),
        GestureDetector(
          child: ListTile(
            leading: Icon(Icons.person),
            title: Text(
              "My Profile",
              style: TextStyle(fontSize: 18, color: Color(0xff323643)),
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => MyProfilePage(),
                ),
              );
            },
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => HistoryPage(),
              ),
            );
          },
          child: ListTile(
            leading: Icon(Icons.history),
            title: Text(
              "Ride History",
              style: TextStyle(fontSize: 18, color: Color(0xff323643)),
            ),
          ),
        ),
        ListTile(
          leading: Icon(Icons.local_offer),
          title: Text(
            "Offers",
            style: TextStyle(fontSize: 18, color: Color(0xff323643)),
          ),
        ),
        ListTile(
          leading: Icon(Icons.notifications),
          title: Text(
            "Notifications",
            style: TextStyle(fontSize: 18, color: Color(0xff323643)),
          ),
        ),
        GestureDetector(
          onTap: () {
            FirebaseAuth.instance.signOut().then((value) {
              Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
            }).catchError((e) {});
          },
          child: ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text(
              "Logout",
              style: TextStyle(fontSize: 18, color: Color(0xff323643)),
            ),
          ),
        ),
      ],
    );
  }
}
