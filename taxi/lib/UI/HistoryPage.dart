import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:taxi/Model/Bird.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('History Page'),
      ),
      body: Container(
        child: Container(
          child: Column(
            children: [
              // CounterWidget(),
              CounterBtn(),
            ],
          ),
        ),
      ),
    );
  }
}

class CounterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Bird bird = Provider.of<Bird>(context);
    // bird.addListener(() { })
    return Scaffold(
      appBar: AppBar(
        title: Text('History'),
      ),
      body: Container(
        child: Column(
          children: [
            Text(bird.qty.toString()),
            // RaisedButton(
            //   onPressed: () {
            //     bird.increment();
            //   },
            // )
          ],
        ),
      ),
    );
  }
}

class CounterBtn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Bird bird = Provider.of<Bird>(context);

    Bird b = new Bird.fromPlayer(10);
    print(b.qty);

    return Column(
      children: [
        Container(
          child: RaisedButton(
            onPressed: () {
              bird.increment();
            },
            child: Text('Increment'),
          ),
        ),
        RaisedButton(onPressed: () {
          Navigator.pushNamed(context, '/CounterWidget');
        })
      ],
    );
  }
}
