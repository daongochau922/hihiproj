import 'package:flutter/material.dart';
import 'package:taxi/BLOC/place_bloc.dart';
import 'package:taxi/Model/PlaceItem.dart';

class RidePickerPage extends StatefulWidget {
  final String selectedAddress;
  final Function(PlaceItem, bool) onSelected;
  final bool _isFromAddress;

  RidePickerPage(this.selectedAddress, this.onSelected, this._isFromAddress);

  @override
  _RidePickerPageState createState() => _RidePickerPageState();
}

class _RidePickerPageState extends State<RidePickerPage> {
  TextEditingController _addressController;
  PlaceBloc _placeBloc = new PlaceBloc();

  @override
  void initState() {
    _addressController =
        new TextEditingController(text: widget.selectedAddress);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: BackButton(
          color: Colors.blue,
        ),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Column(
          children: [
            Row(children: [
              Icon(Icons.location_on),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
              ),
              Expanded(
                child: TextField(
                  controller: _addressController,
                  textInputAction: TextInputAction.search,
                  onSubmitted: (str) {
                    _placeBloc.searchPlace(str);
                  },
                  onChanged: (value) {
                    _placeBloc.searchPlace(value);
                  },
                ),
              ),
              SizedBox(
                width: 50,
                height: 40,
                child: FlatButton(
                  onPressed: () {
                    _addressController.text = '';
                  },
                  child: Container(
                    child: Icon(Icons.clear),
                  ),
                ),
              ),
            ]),
            Expanded(
              child: Container(
                child: StreamBuilder(
                  stream: _placeBloc.placeStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data == 'start') {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      List<PlaceItem> places = snapshot.data;
                      return ListView.separated(
                          itemBuilder: (context, index) {
                            return ListTile(
                              title: Text(places.elementAt(index).title == null
                                  ? ' '
                                  : places.elementAt(index).title),
                              subtitle: Text(places.elementAt(index).url == null
                                  ? ' '
                                  : places.elementAt(index).url),
                              onTap: () {
                                Navigator.pop(context);
                                widget.onSelected(places.elementAt(index),
                                    widget._isFromAddress);
                              },
                            );
                          },
                          separatorBuilder: (context, index) {
                            return Divider(
                              height: 1,
                              color: Color(0xfff5f5f5),
                            );
                          },
                          itemCount: places.length > 50 ? 50 : places.length);
                    } else {
                      return Container();
                    }
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _placeBloc.dispose();
    super.dispose();
  }
}
