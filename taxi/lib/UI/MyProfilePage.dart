import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
// import 'package:taxi/FIREBASE/firebase_auth.dart';

class MyProfilePage extends StatefulWidget {
  @override
  _MyProfilePageState createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage> {
  var nameController = TextEditingController();
  var phoneController = TextEditingController();
  CollectionReference esss = FirebaseFirestore.instance.collection('class');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Profile'),
      ),
      body: Container(
        child: Column(
          children: [
            Row(
              children: [
                Text('Name:'),
                Expanded(
                  child: TextField(
                    controller: nameController,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text('Phone:'),
                Expanded(
                  child: TextField(
                    controller: phoneController,
                  ),
                ),
              ],
            ),
            Center(
              child: RaisedButton(
                onPressed: () {
                  // var authr = FirAuth();
                  // authr.getProfile();
                  addClass(nameController.text,
                      int.parse(phoneController.text.trim()));
                },
                child: Text('Update'),
              ),
            ),
            Center(
              child: StreamBuilder(
                stream: getProfile().onValue,
                builder: (context, snapshot) {
                  // List<User> item = [];

                  // Map data = snapshot.data.snapshot.value;

                  // data.forEach((key, value) => item.add(User.fromJson(value)));

                  // print('ADASD');
                  // print(data);

                  return Text('snapshot.data.snapshot.value');
                },
              ),
            ),
            Expanded(
              child: StreamBuilder<QuerySnapshot>(
                stream: esss.snapshots(),
                builder: (context, stream) {
                  if (stream.connectionState == ConnectionState.waiting) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    print('hehehe');

                    print(stream.data.docs[0].data());

                    return ListView(
                      children:
                          stream.data.docs.map((DocumentSnapshot snapshot) {
                        int qty = 0;
                        try {
                          qty =
                              int.parse(snapshot.data()['classqty'].toString());
                        } catch (e) {
                          qty = 0;
                        }
                        return ListTile(
                          title: Text(snapshot.data()['classname']),
                          subtitle: Text(qty.toString()),
                        );
                      }).toList(),
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  DatabaseReference getProfile() {
    return FirebaseDatabase.instance.reference().child('class');
  }

  void addClass(String classname, int qty) {
    // Lop lop = new Lop(classname, qty);
    FirebaseFirestore.instance
        .collection('class')
        .add({'classname': classname, 'classqty': qty});
  }
}

class Lop {
  String classname;
  int classqty;

  Lop(this.classname, this.classqty);

  Map toJson() => {'classname': classname, 'classqty': classqty};
}

class Users {
  String phone;
  String name;

  Users(this.phone, this.name);

  Map toJson() => {'phone': phone, 'name': name};

  factory Users.fromJson(Map<dynamic, dynamic> json) {
    print(json);
    return Users(json['phone'], json['name']);
  }
}
