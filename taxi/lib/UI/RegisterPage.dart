import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:taxi/BLOC/register_bloc.dart';
import 'package:taxi/UI/HomePage.dart';
import 'package:taxi/UI/DIALOG/LoadingDialog.dart';
import 'package:taxi/UI/DIALOG/MsgDialog.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  Registerbloc registerbloc;

  var userCTL = TextEditingController();

  var passwordCTL = TextEditingController();

  var nameCTL = TextEditingController();

  var phoneCTL = TextEditingController();

  @override
  void initState() {
    registerbloc = Registerbloc();
    super.initState();
  }

  @override
  void dispose() {
    registerbloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.blue,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset(
                'car2.png',
                width: double.infinity,
                height: 100,
                alignment: Alignment.center,
              ),
              Text(
                'Welcome Aboard!',
                style: TextStyle(
                  color: Color(0xff333333),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Text(
                'Signup with iCab with in simple steps',
                style: TextStyle(
                  color: Color(0xff604070),
                  fontSize: 12,
                  fontWeight: FontWeight.w100,
                ),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(50, 30, 50, 0),
                  child: StreamBuilder(
                    stream: registerbloc.nameStream,
                    builder: (context, snapshot) => TextField(
                      controller: nameCTL,
                      decoration: InputDecoration(
                        errorText: snapshot.hasError ? snapshot.error : null,
                        labelText: 'Name',
                        contentPadding:
                            const EdgeInsets.fromLTRB(10, 10, 10, 10),
                        border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xffCE00D2), width: 0.5),
                        ),
                      ),
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 10, 50, 0),
                child: StreamBuilder(
                  stream: registerbloc.phoneStream,
                  builder: (context, snapshot) => TextField(
                    controller: phoneCTL,
                    decoration: InputDecoration(
                      errorText: snapshot.hasError ? snapshot.error : null,
                      labelText: 'Phone number',
                      contentPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffCE00D2), width: 0.5),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 10, 50, 0),
                child: StreamBuilder(
                  stream: registerbloc.emailStream,
                  builder: (context, snapshot) => TextField(
                    controller: userCTL,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffCE00D2), width: 0.5),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 10, 50, 20),
                child: StreamBuilder(
                  stream: registerbloc.passwordStream,
                  builder: (context, snapshot) => TextField(
                    obscureText: true,
                    controller: passwordCTL,
                    decoration: InputDecoration(
                      errorText: snapshot.hasError ? snapshot.error : null,
                      labelText: 'Password',
                      contentPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffCE00D2), width: 0.5),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                child: SizedBox(
                  width: double.infinity,
                  child: RaisedButton(
                    color: Color(0xff3277DB),
                    onPressed: () {
                      print('register');
                      _onSignUpClick();
                    },
                    child: Text(
                      'Signup',
                      style: TextStyle(color: Color(0xfffafafa)),
                    ),
                  ),
                ),
              ),
              RichText(
                text: TextSpan(
                  text: 'Already a user? ',
                  style: TextStyle(
                    color: Color(0xff606470),
                  ),
                  children: [
                    TextSpan(
                        text: 'Login now',
                        style: TextStyle(
                          color: Color(0xff3277DB),
                        ),
                        recognizer: TapGestureRecognizer()..onTap = () {}),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _onSignUpClick() {
    if (registerbloc.isCheckRegister(
            userCTL.text, passwordCTL.text, nameCTL.text, phoneCTL.text) ==
        false) {
      LoadingDialog.showLoadingDialog(context, 'Loading....');
      registerbloc.signUp(
          userCTL.text, passwordCTL.text, nameCTL.text, phoneCTL.text, () {
        print('hello hompe');
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => HomePage(),
          ),
        );
        LoadingDialog.hideLoadingDialog(context);
      }, (err) {
        LoadingDialog.hideLoadingDialog(context);
        print('abcd' + err);
        MsgDialog.showMsgDialog(context, 'Sign-Up', err);
      });
    }
  }
}
