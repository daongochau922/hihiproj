import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:taxi/Model/PlaceItem.dart';

class PlaceService {
  static Future<List<PlaceItem>> searchPlace(String keyword) async {
    var url =
        'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' +
            Uri.encodeQueryComponent(keyword) +
            '&key=AIzaSyAkX6AffDXZMiu73y2GG2mVsrbx9gknvrM';
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return PlaceItem.fromJson(json.decode(response.body));
    } else {
      return new List<PlaceItem>();
    }
  }
}
